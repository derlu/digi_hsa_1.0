require(plyr)
library(readxl)
library(keyring)
library(dplyr)
library(formr)
library(car)


source("/home/derlu/Dokumente/projekte/digi_hsa/digi_hsa_1.0/!TEST/.passwords.R")

#connect to FormR with Username and Password
formr::formr_connect(email = credentials$email,
                     password = credentials$password)

formr::formr_connect(host = "https://formr.hsa.phb.de:8446",
                   email = "l.piccirilli@phb.de",
                   password = "luigi1234")


formr_disconnect(email = "l.piccirilli@phb.de",
                 password = "luigi1234",
                 host = "https://formr.hsa.phb.de:8446/")

formr_item_displays("Base_PT", host = "https://formr.hsa.phb.de:8446/")


formr_items(survey_name = 'Base_PT', host = "https://formr.hsa.phb.de:8446/", path = NULL)

formr_items(survey_name = 'Base_PT')

#get Data FormR

Base_Pt   <- formr_raw_results("Base_PT", host = "https://formr.hsa.phb.de:8446/")
T0_Pt   <- formr_raw_results("T0_PT", host = "https://formr.hsa.phb.de:8446/")
T6_Pt   <- formr_raw_results("T6_PT", host = "https://formr.hsa.phb.de:8446/")
T12_Pt   <- formr_raw_results("T12_PT", host = "https://formr.hsa.phb.de:8446/")
T18_Pt   <- formr_raw_results("T18_PT", host = "https://formr.hsa.phb.de:8446/")
T24_Pt   <- formr_raw_results("T24_PT", host = "https://formr.hsa.phb.de:8446/")
End_Pt   <- formr_raw_results("End_PT", host = "https://formr.hsa.phb.de:8446/")



Base_Pt   <- formr::formr_results("Base_Pt")
T0_Pt     <- formr::formr_raw_results("T0_Pt")
T6_Pt     <- formr::formr_raw_results("T6_Pt")
T12_Pt    <- formr::formr_raw_results("T12_Pt")
T18_Pt    <- formr::formr_raw_results("T18_Pt")
T24_Pt    <- formr::formr_raw_results("T24_Pt")
T30_Pt    <- formr::formr_raw_results("T30_Pt")
End_Pt    <- formr::formr_raw_results("End_Pt")


DF <- function(Base_Pt = formr::formr_raw_results("Base_Pt"), T0_Pt = formr::formr_raw_results("T0_Pt"), T6_Pt = formr::formr_raw_results("T6_Pt"), T12_Pt = formr::formr_raw_results("T12_Pt"), T18_Pt = formr::formr_raw_results("T18_Pt"), T24_Pt = formr::formr_raw_results("T24_Pt"), T30_Pt = formr::formr_raw_results("T30_Pt"), End_Pt = formr::formr_raw_results("End_Pt")) {
  x <- join_all(list(Base_Pt,T0_Pt,T6_Pt,T12_Pt, T18_Pt, T24_Pt, T30_Pt, End_Pt), by = 'session', type = 'full', match = "first")
  load(x)
}

DF()


df <- join_all(list(Base_Pt,T0_Pt,T6_Pt,T12_Pt, T18_Pt, T24_Pt, End_Pt), by = 'session', type = 'full', match = "first")


# merge Dataframes by Chiffre (User-ID) (load from FormR)
df <- join_all(list(formr::formr_raw_results("Base_Pt"), formr::formr_raw_results("T0_Pt"), formr::formr_raw_results("T6_Pt"), formr::formr_raw_results("T12_Pt"), formr::formr_raw_results("T18_Pt"), formr::formr_raw_results("T24_Pt"), formr::formr_raw_results("T30_Pt"), formr::formr_raw_results("End_Pt")), by = 'session', type = 'full', match = "first")
write.csv(df, "~/Dokumente/projekte/digi_hsa/digi_hsa_1.0/!TEST/df.csv")



df_dplyr <- inner_join(Base_Pt,T0_Pt,T6_Pt,T12_Pt,T18_Pt,T24_Pt,End_Pt, by ='session')





infile <- join_all(list(formr::formr_raw_results("Base_Pt"), formr::formr_raw_results("T0_Pt"), formr::formr_raw_results("T6_Pt"), formr::formr_raw_results("T12_Pt"), formr::formr_raw_results("T18_Pt"), formr::formr_raw_results("T24_Pt"), formr::formr_raw_results("T30_Pt"), formr::formr_raw_results("End_Pt")), by = 'session', type = 'full', match = "first")

patient   <- "test"

for (i in 1:21) {
  infile[[paste0("Base_BDI_v", i)]] <- car::recode(infile[[paste0("Base_BDI_v", i)]], "1=0; 2=1; 3=2; 4=3; NA=NA");
  infile[[paste0("End_BDI_v", i)]] <- car::recode(infile[[paste0("End_BDI_v", i)]], "1=0; 2=1; 3=2; 4=3; NA=NA")
}

write.csv(infile, "~/Dokumente/projekte/digi_hsa/digi_hsa_1.0/!TEST/df.csv")

for (i in 1:10) {
  infile[[paste0("Base_PHQ9_v", i)]] <-car::recode(infile[[paste0("Base_PHQ9_v", i)]], "1=0; 2=1; 3=2; 4=3; NA=NA");
  infile[[paste0("T0_PHQ9_v", i)]] <- car::recode(infile[[paste0("T0_PHQ9_v", i)]], "1=0; 2=1; 3=2; 4=3; NA=NA");
  infile[[paste0("T6_PHQ9_v", i)]] <- car::recode(infile[[paste0("T6_PHQ9_v", i)]], "1=0; 2=1; 3=2; 4=3; NA=NA");
  infile[[paste0("T12_PHQ9_v", i)]] <- car::recode(infile[[paste0("T12_PHQ9_v", i)]], "1=0; 2=1; 3=2; 4=3; NA=NA");
  infile[[paste0("T18_PHQ9_v", i)]] <- car::recode(infile[[paste0("T18_PHQ9_v", i)]], "1=0; 2=1; 3=2; 4=3; NA=NA");
  infile[[paste0("End_PHQ9_v", i)]] <- car::recode(infile[[paste0("End_PHQ9_v", i)]], "1=0; 2=1; 3=2; 4=3; NA=NA")
}

for (i in 1:12) {
  infile[[paste0("Base_CROSSD_v", i)]] <- car::recode(infile[[paste0("Base_CROSSD_v", i)]], "1=0; 2=1; 3=2; 4=3; NA=NA");
  infile[[paste0("T0_CROSSD_v", i)]] <- car::recode(infile[[paste0("T0_CROSSD_v", i)]], "1=0; 2=1; 3=2; 4=3; NA=NA");
  infile[[paste0("T6_CROSSD_v", i)]] <- car::recode(infile[[paste0("T6_CROSSD_v", i)]], "1=0; 2=1; 3=2; 4=3; NA=NA");
  infile[[paste0("T12_CROSSD_v", i)]] <- car::recode(infile[[paste0("T12_CROSSD_v", i)]], "1=0; 2=1; 3=2; 4=3; NA=NA");
  infile[[paste0("T18_CROSSD_v", i)]] <- car::recode(infile[[paste0("T18_CROSSD_v", i)]], "1=0; 2=1; 3=2; 4=3; NA=NA");
  infile[[paste0("End_CROSSD_v", i)]] <- car::recode(infile[[paste0("End_CROSSD_v", i)]], "1=0; 2=1; 3=2; 4=3; NA=NA");
}

for (i in 1:53) {
  infile[[paste0("Base_BSI_v", i)]] <- car::recode(infile[[paste0("Base_BSI_v", i)]], "1=0; 2=1; 3=2; 4=3; 5=4; NA=NA");
  infile[[paste0("T6_BSI_v", i)]] <- car::recode(infile[[paste0("T6_BSI_v", i)]], "1=0; 2=1; 3=2; 4=3; 5=4; NA=NA");
  infile[[paste0("T12_BSI_v", i)]] <- car::recode(infile[[paste0("T12_BSI_v", i)]], "1=0; 2=1; 3=2; 4=3; 5=4; NA=NA");
  infile[[paste0("T18_BSI_v", i)]] <- car::recode(infile[[paste0("T18_BSI_v", i)]], "1=0; 2=1; 3=2; 4=3; 5=4; NA=NA");
  infile[[paste0("End_BSI_v", i)]] <- car::recode(infile[[paste0("End_BSI_v", i)]], "1=0; 2=1; 3=2; 4=3; 5=4; NA=NA")
}

for (i in c(2,5,7,13,19,26,28)) {
  infile[[paste0("Base_CTQ_v", i)]] <- car::recode(infile[[paste0("Base_CTQ_v", i)]], "1=5; 2=4; 3=3; 4=2; 5=1; NA=NA");
  infile[[paste0("End_CTQ_v", i)]] <- car::recode(infile[[paste0("End_CTQ_v", i)]], "1=5; 2=4; 3=3; 4=2; 5=1; NA=NA")
}

for (i in c(10,16,22)) {
  infile[[paste0("Base_CTQ_v", i)]] <- car::recode(infile[[paste0("Base_CTQ_v", i)]], "1=0; 2=0; 3=0; 4=0; 5=1; NA=NA");
  infile[[paste0("End_CTQ_v", i)]] <- car::recode(infile[[paste0("End_CTQ_v", i)]], "1=0; 2=0; 3=0; 4=0; 5=1; NA=NA")
}

ss_pupo <- subset(pupo, select = c(Base_BDI_v1:Base_BDI_v21))

pupo <- subset(infile, Chiffre == "p010101")


stringr::str_sub(infile$session, 1, 8)


substr("12345PLXXX3nbpJWMkVetLM1DtT3ezu9YZA8pefobefpSuvHOc8n3tMjxt-scdqc", 1,7)
substring("abcdef", 1:6, 1:6)
