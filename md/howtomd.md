### Anleitung zum Schreiben der Notizen

Hier wird ein kurzer Überblick über das Schreiben der Notizen gegeben, die Sie im Reiter *Ergebnisse* im unteren Textfeld eingeben
können. Die Notizen sollen die Möglichkeit bieten, wichtige Anmerkungen zu den Testergebnissen festzuhalten.
Die Strukturierung der Notizen erfolgt über kleine, unkomplizierte *Markdownbefehle*, mit denen auf einfache Art und Weise Texte formatiert und gegliedert werden können. Die Syntax der Markdownbefehle ist sehr simpel und kann nach einer kurzen Einarbeitung effizient genutzt werden.
***
### Markdown-Befehle nutzen 

Hier werden in Kürze nur die wichtigsten Markdownbefehle dargestellt. Natürlich können Sie die Notizen auch ohne jegliche Formatierung schreiben. Diese Anleitung soll lediglich eine Hilfestellung sein, wenn Sie z.B. direkt etwas *kursiv schreiben* möchten.


Wenn Sie einen Absatz in Ihre Notizen einbauen möchten, dann beenden Sie die Zeile, nach der der Absatz erfolgen soll, indem Sie *zwei Mal auf Enter* klicken.  
Um einen Zeilenumbruch zu erstellen, beenden Sie die Zeile, nach der der Umbruch erfolgen soll, mit *zwei Leerzeichen* und *einem Klick auf Enter*.


Die Gestalt Ihrer Notizen können Sie wie folgt verändern:
<br>
> **fett gedruckter Text** = ```**fett gedruckter Text**```  
*kursiver Text* = ```*kursiver Text*```  
~~durchgestrichener Text~~ = ```~~durchgestrichener Text~~```


Sie können über das Symbol der Raute bis zu sechs Ebenen von Überschriften erstellen, indem Sie vor die Überschrift eine # für eine Überschrift auf Ebene 1 setzen, ## für eine Überschrift auf Ebene 2, ### für eine Überschrift auf Ebene 3 usw. Dabei steht je ein Leerzeichen zwischen den Rauten und dem Text der Überschrift.
<br>
> # Überschrift Ebene 1
## Überschrift Ebene 2
### Überschrift Ebene 3
#### Überschrift Ebene 4


Ebenso können ungeordnete und nummerierte Listen durch Voranstellung von Satzzeichen erstellt werden. Eine ungeordnete Aufzählungsliste wird mithilfe der Symbole +, * oder - erstellt. Vor jeden Punkt der Liste wird in einer neuen Zeile eins der Symbole gesetzt.
<br>
> + Punkt 1    = ``` + Punkt 1 ```
+ Punkt 2      = ``` * Punkt 2 ```
- Punkt 3      = ``` - Punkt 3 ```


Eine nummerierte Liste kann ganz einfach durch die Ordinalzahlen erstellt werden:
<br>
> 1. Punkt    = ```1. Punkt ```
1. Punkt      = ```1. Punkt ``` (die Nummerierung ist egal, sie wird automatisch fortgesetzt)
2. Punkt      = ```2. Punkt ``` 
9. Punkt      = ```9. Punkt ```

