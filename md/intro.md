## Digitale Diagnostik an der HSA
***

### Was kann diese App?

Diese Applikation ist ein Shiny-Programm, welches die Auswertung in der Diagnostik der relevanten Instrumente im Zug der Therapie an der HSA der PHB erleichtern und automatisieren soll. Entstanden ist dies anfänglich während meines Praktikums an der Hochschulambulanz und wurde im Zuge meiner Tätigkeit als studentischer Mitarbeiter der HSA vervollständigt.

<br>

Mithilfe des Programms können sowohl Therapeuten wie auch PiA anhand des Datensatzes der ausgefüllten Fragebögen der Patienten durch Auswahl des gewünschten Messzeitpunktes und Eingabe der Patientenchiffre die für die Therapie relevanten Testergebnisse einsehen.

<br> 

### Login?

Sollten Sie zur Benutzung des Programms berechtigt sein, geben Sie bitte rechts die Ihnen zugewiesenen Login-Daten ein. Sollten Sie Ihre Zugangsdaten 3x falsch eingegeben haben, wird Ihr Account gesperrt. Wenden Sie sich in diesem Fall an l.piccirilli@psychologische-hochschule.de.

<br> 

Sollten Sie noch keine Login-Daten erhalten haben, wenden Sie sich bitte ebenfalls an l.piccirilli@psychologische-hochschule.de  


