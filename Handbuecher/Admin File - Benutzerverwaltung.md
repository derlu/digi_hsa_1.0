---
date_created: 2023-11-05
date_modified: 2023-11-05
---
Der Code enthält Funktionen zum Verwalten von Benutzern und Passwörtern für die Shiny-Anwendung. Diese Funktionen ermöglichen das Erstellen und Verwalten von Anmeldeinformationen für die Benutzer der Shiny-App.

1. `credentials_init()`

   Diese Funktion erstellt ein Verzeichnis "credentials" im aktuellen Arbeitsverzeichnis und eine leere Datenrahmen-Datei "credentials.rds", in der die Benutzeranmeldeinformationen gespeichert werden. Wenn das Verzeichnis oder die Datei bereits vorhanden ist, wird eine Fehlermeldung ausgegeben.

   Beispiel:

   ```R
   credentials_init()
   ```

2. `add_users(users, pws)`

   Diese Funktion fügt Benutzer zu der in "credentials.rds" gespeicherten Datenrahmen-Datei hinzu. Sie erwartet zwei Eingabevektoren: `users` enthält die Benutzernamen und `pws` die zugehörigen Passwörter. Beachten Sie, dass die beiden Eingabevektoren in der gleichen Reihenfolge angeordnet sein müssen.

   Beispiel:

   ```R
   add_users("user1", "password1")
   ```

   Oder für mehrere Benutzer:

   ```R
   users_to_add <- c("user2", "user3")
   passwords_to_add <- c("password2", "password3")
   add_users(users_to_add, passwords_to_add)
   ```

3. `delete_users(users)`

   Diese Funktion löscht Benutzer aus der "credentials.rds"-Datei. Sie erwartet einen Eingabevektor `users`, der die zu löschenden Benutzernamen enthält.

   Beispiel:

   ```R
   delete_users("user1")
   ```

   Oder für mehrere zu löschende Benutzer:

   ```R
   users_to_delete <- c("user1", "user2")
   delete_users(users_to_delete)
   ```

Die Funktionen verwenden eine Datenrahmen-Datei "credentials.rds" zum Speichern von Benutzerinformationen. Die Passwörter werden mithilfe der `digest`-Funktion gehasht, um die Sicherheit zu gewährleisten.

Die Funktionen überprüfen auch verschiedene Bedingungen, z.B. ob die Benutzernamen eindeutig sind, ob es Benutzer mit den gleichen Namen gibt, ob die Eingaben gültig sind usw. Wenn Bedingungen nicht erfüllt sind, werden Fehler- oder Warnmeldungen generiert.

In Ihrem R-Code gibt es keinen direkten Mechanismus, um zu überprüfen, ob bereits ein Benutzer mit einem bestimmten Benutzernamen existiert. Sie können jedoch über die Funktion `add_users` im Code prüfen, ob ein Benutzername bereits in Ihrem `credentials.rds`-Datenrahmen vorhanden ist, bevor Sie einen neuen Benutzer hinzufügen.

Hier ist die relevante Stelle im Code, wo die Überprüfung erfolgen kann:

```R
# add users
credentials <- readRDS("credentials/credentials.rds")

if (any(credentials[, "user"] %in% users)) {
  dupe_users <- credentials[which(credentials[, "user"] %in% users), "user"]

  dupe_users <- paste(dupe_users, collapse = ", ")
  message <- paste0("Users [", dupe_users, "] already exist - choose different user names.")
  stop(message)
}
```

In diesem Abschnitt des Codes wird überprüft, ob die im Parameter `users` übergebenen Benutzernamen bereits im `credentials.rds`-Datenrahmen vorhanden sind. Wenn ja, wird eine Fehlermeldung erzeugt, die besagt, dass Benutzer mit den angegebenen Benutzernamen bereits existieren. Dies dient dazu, sicherzustellen, dass keine Duplikate bei der Erstellung neuer Benutzerkonten erstellt werden.
