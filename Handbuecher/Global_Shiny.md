---
date_created: 2023-11-05
date_modified: 2023-11-05
---

1. **Benötigte R-Pakete definieren:**
   In dieser Zeile wird ein Vektor namens `packages` erstellt, der die Namen der R-Pakete enthält, die im folgenden Code benötigt werden. 
```R
# Benötigte R-Pakete definieren
packages = c("formr","plyr","hrbrthemes","tinytex","noteMD","png","car","haven","shiny", "shinyalert", "shinycssloaders", "digest", "ggplot2","psych", "sjPlot", "markdown", "rmarkdown",  "shinythemes", "DT",  "shinyjs", "shinyjqui", "knitr", "grid","gridExtra" )
```   
   
   Diese Pakete sind notwendig, um die Funktionalitäten der Shiny-App und anderer Aufgaben im Code zu unterstützen:

1. `formr`: Dieses Paket ermöglicht den Zugriff auf die formr-Plattform, die für die Erstellung von Online-Fragebögen und das Sammeln von Forschungsdaten verwendet wird. Es bietet Funktionen zum Herstellen und Verwalten einer Verbindung zur formr-Plattform.

2. `plyr`: Dieses Paket stellt Funktionen für die Datenmanipulation zur Verfügung, insbesondere für das Umschichten und Zusammenfassen von Daten. Es erleichtert komplexe Datenmanipulationen.

3. `hrbrthemes`: `hrbrthemes` ist ein Paket für die Gestaltung und Anpassung von Grafiken und Diagrammen in R. Es bietet eine breite Palette von Themen und Optionen für die grafische Gestaltung.

4. `tinytex`: Dieses Paket bietet Werkzeuge für die Verwendung von LaTeX in R. Es erleichtert die Installation und Verwendung von LaTeX sowie das Erstellen von LaTeX-Dokumenten in R.

5. `noteMD`: `noteMD` ist ein Paket, das es ermöglicht, Notizen und Anmerkungen in R Markdown-Dokumenten zu erstellen und darzustellen. Es erleichtert die Integration von Kommentaren und Notizen in R Markdown-Dateien.

6. `png`: Dieses Paket bietet Funktionen zum Erstellen von PNG-Bildern aus R-Grafiken und Diagrammen. Es ist nützlich, wenn Sie R-Grafiken in anderen Anwendungen verwenden möchten.

7. `car`: Das `car`-Paket (Companion to Applied Regression) bietet Funktionen für die Regression und Modellierung von Daten. Es enthält viele nützliche Funktionen für statistische Analysen.

8. `haven`: `haven` ist ein Paket zur Arbeit mit Daten, die im SPSS- oder SAS-Dateiformat vorliegen. Es ermöglicht das Einlesen und Schreiben dieser Datenformate in R.

9. `shiny`: Dieses Paket ist das Herzstück für die Erstellung von interaktiven Webanwendungen in R. Shiny ermöglicht die einfache Erstellung von Benutzeroberflächen und das Einbetten von R-Code in Webanwendungen.

10. `shinyalert`: `shinyalert` ist ein Paket, das Popup-Benachrichtigungen und Meldungen in Shiny-Apps ermöglicht. Dies erleichtert die Kommunikation mit Benutzern.

11. `shinycssloaders`: Dieses Paket bietet Ladeanzeigen und Animationen für Shiny-Apps. Es verbessert die Benutzerfreundlichkeit, indem es anzeigt, dass die App Daten lädt oder verarbeitet.

12. `digest`: `digest` ist ein Paket zur Erstellung von Hash-Funktionen. Es kann zur Verschlüsselung von Passwörtern und zur Prüfung der Integrität von Daten verwendet werden.

13. `ggplot2`: `ggplot2` ist eines der bekanntesten R-Pakete für die Datenvisualisierung. Es ermöglicht die Erstellung von ansprechenden und flexiblen Grafiken.

14. `psych`: `psych` ist ein Paket für die Psychometrie und die Durchführung von psychologischen Analysen. Es bietet Tools zur Durchführung von Faktorenanalysen und anderen psychometrischen Analysen.

15. `sjPlot`: Dieses Paket bietet Funktionen zur Erstellung von Tabellen und Grafiken aus statistischen Modellen in R. Es ist nützlich für die Präsentation von Analyseergebnissen.

16. `markdown`: `markdown` ist ein Paket zur Konvertierung von R-Markdown-Dokumenten in andere Formate wie HTML oder PDF. Es ist hilfreich, um R-Markdown-Dateien zu rendern.

17. `rmarkdown`: `rmarkdown` ist ein Paket, das die Erstellung von dynamischen R-Markdown-Dokumenten ermöglicht. Es kombiniert R-Code mit Text und Grafiken in einem einzigen Dokument.

18. `shinythemes`: `shinythemes` bietet zusätzliche Themen und Anpassungsmöglichkeiten für Shiny-Apps. Es ermöglicht die Gestaltung des Erscheinungsbilds Ihrer Shiny-App.

19. `DT`: `DT` ist ein Paket zur Erstellung interaktiver HTML-Tabellen aus R-Datenrahmen. Es ist ideal für die Präsentation von Daten in Shiny-Apps.

20. `shinyjs`: `shinyjs` ist ein Paket, das JavaScript-Funktionen und -Effekte in Shiny-Apps integriert. Es ermöglicht die Erweiterung der Interaktivität Ihrer Apps.

21.  `shinyjqui`: Dieses Paket stellt Funktionen für jQuery-Widgets in Shiny-Apps bereit. Es erweitert die Auswahl an Widgets und Interaktionen für Benutzer.

22. `knitr`: `knitr` ist ein Paket zur dynamischen Dokumenterstellung. Es ermöglicht das Einbetten von R-Code und -Ergebnissen in Dokumente.

23. `grid` und `gridExtra`: Diese Pakete bieten Funktionen zur Erstellung und Anpassung von R-Grafiken und Layouts. Sie ermöglichen die präzise Gestaltung von Grafiken und Diagrammen.


2. **Überprüfung und Installation von R-Paketen:**
   Hier wird die `package.check`-Funktion definiert, die alle im Vektor `packages` aufgeführten R-Pakete überprüft und installiert, wenn sie nicht bereits installiert sind. Dies geschieht mithilfe von `lapply`, das die Funktion `require` auf jedes Paket im Vektor anwendet. Wenn `require` ergibt, dass ein Paket nicht installiert ist, wird es mithilfe von `install.packages` installiert und dann mit `library` geladen. Dies stellt sicher, dass alle benötigten Pakete verfügbar sind.

Überprüfung und Installation von R-Paketen

```R
# Überprüfung und Installation von R-Paketen
package.check <- lapply(packages, FUN = function(x) {
  if (!require(x, character.only = TRUE)) {
    install.packages(x, dependencies = TRUE)
    library(x, character.only = TRUE)
  }
})
```
3. **Anmeldeinformationen festlegen:**
   In den Zeilen, die mit `host`, `email` und `password` beginnen, werden die Anmeldeinformationen für die externe Datenquelle (hier "formr") festgelegt. Dazu gehören die Host-URL, die E-Mail-Adresse und das Passwort. Diese Informationen werden für die Verbindung zur externen Datenquelle verwendet.
```R
# Anmeldeinformationen festlegen
host <- "https://formr.hsa.phb.de"
email <- "l.piccirilli@phb.de"
password <- "luigi1234"
```

4. **Verbindung zur externen Datenquelle (formr) herstellen:**
   In diesem Teil wird eine Verbindung zur externen Datenquelle hergestellt. Dies erfolgt mithilfe der `formr_connect`-Funktion aus dem "formr"-Paket. Ein `tryCatch`-Block wird verwendet, um auf mögliche Fehler während der Verbindungsherstellung zu reagieren. Wenn ein Fehler auftritt, wird eine Fehlermeldung mit einer entsprechenden Meldung ausgegeben.
```R
# Verbindung zur externen Datenquelle (formr) herstellen
tryCatch({
  formr::formr_connect(host = host, email = email, password = password)
}, error = function(e) {
  # Fehlerbehandlung
  message("Fehler bei der Verbindung zu formr: ", e$message)
})
```

5. **Codierungseinstellungen:**
   Hier wird die Codierung für die Shiny-App festgelegt. Die `getOption`-Funktion wird verwendet, um die Einstellung für die Codierung zu ermitteln. Die Standardeinstellung ist "latin1", falls keine spezifische Einstellung festgelegt wurde.
```R
# Codierungseinstellungen
encoding <- getOption("shiny.site.encoding", default = "latin1")
```

6. **Optionen für das R-Markdown-Knitting/Rendering:**
   Dieser Abschnitt enthält Konfigurationsoptionen für das Knitting und Rendering von R-Markdown-Dokumenten. Die `knitr::opts_chunk$set`-Funktion ermöglicht die Festlegung von Optionen, um beispielsweise den Echo-Modus, Kommentare und das Caching von Code-Chunks zu steuern.
```R
# Optionen für das R-Markdown-Knitting/Rendering
knitr::opts_chunk$set(
  echo = FALSE,
  comment = NA,
  cache = FALSE,
  message = FALSE,
  warning = FALSE
)
```

7. **Funktionen zum Rendern von Markdown- und R-Markdown-Dateien:**
   Hier werden zwei Funktionen definiert, um Textdateien in HTML zu konvertieren. `inclMD` dient zum Rendern von Markdown-Dateien, während `inclRmd` für R-Markdown-Dateien verwendet wird. Sie nehmen Dateipfade als Eingabe und geben HTML-Code aus. Die Funktion `make_table` erstellt HTML-Tabellen aus Data Frames.
```R
# Funktion zum Rendern von R-Markdown-Dateien zu HTML
inclRmd <- function(path, r_env = parent.frame()) {
  paste(
    readLines(path, warn = FALSE, encoding = encoding),
    collapse = '\n'
  ) %>%
    knitr::knit2html(
      text = .,
      fragment.only = TRUE,
      envir = r_env,
      options = "",
      stylesheet = "",
      encoding = encoding
    ) %>%
    gsub("&lt;!--/html_preserve--&gt;","",.) %>%  ## knitr adds this
    gsub("&lt;!--html_preserve--&gt;","",.) %>%   ## knitr adds this
    HTML
}

# Funktion zur Erstellung von HTML-Tabellen aus Data Frames
make_table <- function(dat, width = "50%") {
  knitr::kable(
    dat,
    align = "c",
    format = "html",
    table.attr = paste0("class='table table-condensed table-hover' style='width:", width, ";'")
  )
}

```

8. **Einstellung für die Anzahl der zulässigen fehlgeschlagenen Anmeldeversuche:**
   In dieser Zeile wird die Variable `num_fails_to_lockout` festgelegt, die die Anzahl der fehlgeschlagenen Anmeldeversuche angibt, bevor ein Benutzer gesperrt wird.
```R
# Einstellung für die Anzahl der zulässigen fehlgeschlagenen Anmeldeversuche
num_fails_to_lockout <- 3
```

Der gesamte Code zielt darauf ab, sicherzustellen, dass alle erforderlichen R-Pakete installiert und geladen sind, Anmeldeinformationen für Datenquelle auf FormR bereitgestellt werden, R-Markdown-Knitting-Einstellungen konfiguriert sind und Funktionen zum Rendern von Texten und Tabellen in HTML zur Verfügung stehen. Die Einstellung `num_fails_to_lockout` ist wichtig, um das Verhalten der Anmeldefunktion in der Shiny-App zu steuern.
