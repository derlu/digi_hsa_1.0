---
date_created: 2023-11-05
date_modified: 2023-11-05
---

**shinyServer-Funktion**: Dies ist die Hauptfunktion, die den R-Shiny-Server definiert. In diesem Teil wird die gesamte Serverlogik für die Webanwendung erstellt. Es reagiert auf Benutzereingaben und generiert dynamische Ausgaben.

**Globaler UI-Code**: Hier wird der UI-Teil (Benutzeroberfläche) definiert, der auf der Benutzerseite angezeigt wird. Die Benutzeroberfläche wird dynamisch erstellt, abhängig davon, ob der Benutzer authentifiziert ist oder nicht.

**Bedingte Anzeige des UI**: Der Code überprüft, ob der Benutzer authentifiziert ist (durch `user_input$authenticated`) und zeigt entweder die Anmelde-Oberfläche oder die Diagnose-Oberfläche an.

**Diagnose-Oberfläche**: Wenn der Benutzer authentifiziert ist, wird eine Registerkarte "Diagnostik an der HSA" mit verschiedenen Registerkarten erstellt, die jeweils spezifische Inhalte anzeigen. Diese Registerkarten enthalten Informationen und Interaktionselemente zur Analyse von Patientendaten.

**Zeitreihen-Tab**: Dieser Tab zeigt Zeitreihen-Daten für verschiedene Messinstrumente an.

**Rohdaten der Fragebögen-Tab**: Hier werden die Rohdaten der Fragebögen angezeigt.

**Normwerte-Tab**: Dieser Tab zeigt Normwerttabellen für verschiedene Tests und Subskalen an.

**Manual Markdown-Tab**: Dieser Tab enthält Anleitungen zur Verwendung von Markdown für die Benutzer.

**Ausloggen-Button**: Der "Ausloggen"-Button ermöglicht dem Benutzer das Abmelden von der Anwendung.

Der Code verwendet Shiny-Elemente wie `renderUI`, `renderMarkdown`, `renderDataTable`, und andere, um dynamische Inhalte zu generieren, die auf Benutzerinteraktionen reagieren. Es gibt auch einige UI-Elemente wie `textInput`, `downloadButton`, und `radioButtons`, die es dem Benutzer ermöglichen, Eingaben zu machen und Aktionen auszuführen.

```R
shinyServer(function(input, output, session) {
```

Dies ist der Beginn der `shinyServer`-Funktion, die die Serverseite der R-Shiny-Webanwendung definiert. Hier wird die Serverlogik festgelegt, die auf Benutzeraktionen reagiert und dynamische Ausgaben generiert.

```R
output$ui <- renderUI({
```

Hier wird ein dynamischer Teil der Benutzeroberfläche (`ui`) definiert. Abhängig von den Bedingungen wird unterschiedlicher UI-Code generiert.

```R
if (user_input$authenticated == FALSE) {
```

Hier wird geprüft, ob der Benutzer nicht authentifiziert ist.

```R
fluidPage(
  useShinyjs(),
  theme = shinytheme("cerulean"),
```

Hier wird eine `fluidPage` erstellt, die eine flexible, anpassbare Seite für die Benutzeroberfläche darstellt. `useShinyjs` aktiviert die Verwendung von Shinyjs, einer Erweiterung für Shiny, und `shinytheme` legt das verwendete Design-Thema fest (in diesem Fall "cerulean").

```R
fluidRow(
  sidebarPanel(
    column(width = 12,
      includeMarkdown("./md/intro.md")
    ),
    hr()
  ),
  mainPanel(
    column(width = 7, offset = 2,
      br(),
      br(),
      br(),
      br(),
      h1("Bitte Logindaten eingeben"),
      uiOutput("uiLogin"),
      uiOutput("pass")
    )
  )
)
```

Hier wird die Benutzeroberfläche für den Fall erstellt, dass der Benutzer nicht authentifiziert ist. Es besteht aus einer Seitenleiste (`sidebarPanel`) und einem Hauptbereich (`mainPanel`). In der Seitenleiste wird ein Markdown-Dokument (`intro.md`) eingebettet, das den Benutzer begrüßt. Im Hauptbereich werden UI-Elemente wie `uiLogin` und `pass` (wahrscheinlich Texteingabefelder) generiert.

```R
} else if (user_input$authenticated == TRUE) {
```

Hier wird geprüft, ob der Benutzer authentifiziert ist.

```R
navbarPage(
  theme = shinytheme("cerulean"),
  "Diagnostik an der HSA",
  id = "start",
  selected = "Intro",
  collapsible = TRUE,
  fluid = TRUE,
  position = c("static-top"),
  tabPanel("Intro",
    includeMarkdown("./md/intro_main.md"),
    hr()
  ),
```

Hier wird die Benutzeroberfläche für den Fall erstellt, dass der Benutzer authentifiziert ist. Es handelt sich um eine `navbarPage`, die eine Navigationsleiste mit verschiedenen Registerkarten (`tabPanel`) erstellt. Jede Registerkarte enthält unterschiedliche Inhalte, die durch Markdown-Dokumente (`intro_main.md`) dargestellt werden.

Dieser Prozess setzt sich fort und erstellt Registerkarten für "Patientendaten einlesen", "Ergebnisse", "Zeitreihe", "Rohdaten der Fragebögen", "Normwerte" und "Manual Markdown". Jede Registerkarte hat spezifische Inhalte und Interaktionselemente.

```R
fluidRow(
  column(6, h3("Notizen", align = "center"),
    helpText("Achtung: Alles, was Sie hier eintippen, wird beim Download des Reports ausgedruckt. Rechts sehen Sie eine Vorschau Ihres Textes.")
  ),
  column(12,
    column(6, tags$textarea("Nutzen Sie hier **markdown**-Befehle für Ihre Notizen!", id = 'markdowninput', rows = 3, style = 'width:100%;')),
    column(6, htmlOutput('htmlmarkdown'))
  )
)
```

Hier wird ein Abschnitt für die Benutzer-Notizen erstellt. Der Benutzer kann Markdown verwenden, um Notizen einzugeben, und die HTML-Ausgabe der Notizen wird auf der rechten Seite angezeigt.

```R
observe({
    toggle(condition = input$summary, selector = "#start li a[data-value=summary]")
})
```

Hier handelt es sich um einen Beobachter (observer) in Shiny. Dieser wird verwendet, um auf Änderungen von Eingabewerten zu reagieren. In diesem Fall wird der Beobachter aktiviert, wenn sich der Wert von `input$summary` ändert. Wenn `input$summary` wahr ist, wird die Funktion `toggle` aufgerufen. Diese Funktion ändert den Zustand eines Elements auf der Benutzeroberfläche basierend auf einem angegebenen CSS-Selektor. In diesem Fall wird das Element mit dem Selector `"#start li a[data-value=summary]"` ein- oder ausgeschaltet, je nachdem, ob `input$summary` wahr oder falsch ist.

```R
datensatz_patient <- reactive({
    # URLs definieren für den Zugriff auf den FormR-Server
    urls <- c("BaselinePart1", "BaselinePart2", "T0_Pt", "T6_Pt", "T12_Pt", "T18_Pt", "T24_Pt", "End_Pt")
    host <- "https://formr.hsa.phb.de/"
    
    # Ergebnisse zusammenführen
    infile <- join_all(
        lapply(urls, function(url) formr_raw_results(url, host = host)),
        by = "session",
        type = "full",
        match = "first"
    )
    
    validate(
        need(input$pchiffre %in% infile$pid, "Patient*in mit dieser Chiffre existiert nicht. Bitte überprüfen Sie die eingegebene Chiffre. Diese muss die Form: p010101 haben")
    )
    patient <- input$pchiffre
    
    # Recodierung für BDI
    # Hier wird der BDI-Score für verschiedene Zeitpunkte umkodiert.
    # Die Umkodierung erfolgt mithilfe der Funktion 'recode' aus dem 'car'-Paket.
    # Zum Beispiel wird der Wert 1 auf 0, der Wert 2 auf 1 und so weiter umkodiert.
    
    for (i in 1:15) {
        infile[[paste0("Base_BDI_v", i)]] <- car::recode(infile[[paste0("Base_BDI_v", i)]], "1=0; 2=1; 3=2; 4=3; NA=NA");
        infile[[paste0("End_BDI_v", i)]] <- car::recode(infile[[paste0("End_BDI_v", i)]], "1=0; 2=1; 3=2; 4=3; NA=NA")
    }
    
    # Weitere Umkodierungen für BDI-Scores erfolgen ähnlich.
    
    # Recodierung für PHQ9, CROSSD und BSI-Scores, sowie CTQ
    # Hier erfolgt eine Umkodierung für diese verschiedenen Fragebögen, ähnlich wie beim BDI.
    
    # Subset für ausgewählte Chiffre
    subset(infile, pid == patient)
})
```

In diesem Abschnitt wird ein reaktiver Ausdruck `datensatz_patient` erstellt. Dieser reaktive Ausdruck verwendet die Eingabe (`input$pchiffre`), um die Daten für einen bestimmten Patienten aus den FormR-URLs abzurufen und diese Daten anschließend zu recodieren. Es wird sichergestellt, dass der angegebene Wert für `input$pchiffre` in den geladenen Daten vorhanden ist. Anschließend erfolgt die Umkodierung der Werte für verschiedene Fragebögen wie BDI, PHQ9, CROSSD, BSI und CTQ. Das Ergebnis ist ein Teildatensatz (`infile`), der nur die Daten für den ausgewählten Patienten enthält.

```R
messzeitpunkt <- reactive(input$messzeitpunkt)
geschlecht <- reactive(input$geschlecht)
format.down <- reactive(input$format-down)
```

Diese Zeilen erstellen weitere reaktive Ausdrücke für die Eingaben des Benutzers, darunter `messzeitpunkt`, `geschlecht` und `format.down`. Diese reaktiven Ausdrücke erfassen die Auswahl des Benutzers für den Messzeitpunkt, das Geschlecht und das Format, das für den Download verwendet werden soll.

```R
BDI2_dat <- reactive({
    if (messzeitpunkt() == "base") {
        subset(datensatz_patient(), select = c(Base_BDI_v1:Base_BDI_v21))
    } else if (messzeitpunkt() == "end") {
        subset(datensatz_patient(), select = c(End_BDI_v1:End_BDI_v21))
    } else {
        NULL
    }
})
```

In diesem Abschnitt wird ein reaktiver Ausdruck `BDI2_dat` erstellt, der die Daten des BDI-II-Fragebogens basierend auf dem ausgewählten Messzeitpunkt (`messzeitpunkt`) aus dem Teildatensatz `datensatz_patient` extrahiert. Je nachdem, ob "base" oder "end" als Messzeitpunkt ausgewählt ist, werden die entsprechenden Spalten ausgewählt und in `BDI2_dat` zurückgegeben. Wenn ein anderer Wert ausgewählt ist, wird `NULL` zurückgegeben.

<u>Hier folgen die Erläuterungen zu dem Code der einzelnen Berechnungen der Fragebögen:</u>

**PHQ-9, CrossD, BSI und CTQ Daten laden:**
- Es gibt vier Abschnitte im Code (PHQ-9, CrossD, BSI und CTQ), die jeweils spezifische Daten aus einem Datensatz abrufen, abhängig von einem bestimmten "messzeitpunkt". Dieser Zeitpunkt kann "base", "t0", "t6", "t12", "t18" oder "end" sein.
- Die `reactive`-Funktion stellt sicher, dass die abgerufenen Daten immer automatisch aktualisiert werden, wenn sich der Wert des "messzeitpunkt" ändert.

**Berechnung von BSI-Subskalen:**
- Für jede BSI-Subskala werden `reactive`-Funktionen verwendet, um die entsprechenden Spalten aus den BSI-Daten zu extrahieren.
- Jede Subskala enthält bestimmte BSI-Items, die zu dieser Subskala gehören.

**Berechnung von fehlenden Werten:**
- Es werden `reactive`-Funktionen verwendet, um die Anzahl der fehlenden Werte für verschiedene Datensätze zu berechnen, einschließlich BDI2, PHQ-9, CrossD, BSI-Subskalen und CTQ.

**Berechnung von BDI2, PHQ9, CrossD und BSI Summenwerten:**
- Es gibt `reactive`-Funktionen, die die Summe der Werte für BDI2, PHQ-9, CrossD und BSI-Daten berechnen.

**Berechnung von BSI-Skalen und T-Werten:**
- Für jede BSI-Subskala werden `reactive`-Funktionen verwendet, um Skalenwerte (G(n), gerundet auf 2 Dezimalstellen) zu berechnen.
- Es gibt auch `reactive`-Funktionen, die die Skalenwerte in T-Werte umkodieren, basierend auf dem Geschlecht der Person.

**Berechnung von BSI globalen Kennwerten:**
- Es gibt `reactive`-Funktionen, die den Gesamtscore (GSI) für BSI berechnen, indem sie die Summe der Skalenwerte verwenden.
- Es wird auch die Posttraumatische Symptomdiskrepanz (PSDI) berechnet.

Der Code führt eine Vielzahl von Berechnungen und Datenauswahl für verschiedene Datensätze und Subskalen durch und berücksichtigt dabei den Messzeitpunkt und das Geschlecht der Person. Die Verwendung von `reactive`-Funktionen gewährleistet, dass die Ergebnisse automatisch aktualisiert werden, wenn sich die relevanten Daten oder Parameter ändern.


1. **PHQ-9 und andere Datensätze auswählen**:

   Dieser Teil des Codes verwendet `reactive`-Funktionen, um Datensätze basierend auf dem Messzeitpunkt auszuwählen. Wenn der Messzeitpunkt "base", "t0", "t6", "t12", "t18" oder "end" ist, wird der entsprechende Ausschnitt des Datensatzes ausgewählt. Andernfalls wird `NULL` zurückgegeben.

   Hier ist ein Beispiel für den Code:

   ```r
   PHQ9_dat <- reactive({
     if (messzeitpunkt() == "base") {
       subset(datensatz_patient(), select = c(Base_PHQ9_v1:Base_PHQ9_v10))
     } else if (messzeitpunkt() == "t0") {
       subset(datensatz_patient(), select = c(T0_PHQ9_v1:T0_PHQ9_v10))
     } else if (messzeitpunkt() == "t6") {
       subset(datensatz_patient(), select = c(T6_PHQ9_v1:T6_PHQ9_v10))
     } else if (messzeitpunkt() == "t12") {
       subset(datensatz_patient(), select = c(T12_PHQ9_v1:T12_PHQ9_v10))
     } else if (messzeitpunkt() == "t18") {
       subset(datensatz_patient(), select = c(T18_PHQ9_v1:T18_PHQ9_v10))
     } else if (messzeitpunkt() == "end") {
       subset(datensatz_patient(), select = c(End_PHQ9_v1:End_PHQ9_v10))
     } else {
       NULL
     }
   })
   ```

   Der Code verwendet `subset`, um die gewünschten Spalten des Datensatzes abhängig vom Messzeitpunkt auszuwählen. Diese Funktion wählt die relevanten Spalten aus, basierend auf dem Wert von `messzeitpunkt()`.

2. **Weitere Datensätze auswählen**:

   Der Code wiederholt den vorherigen Abschnitt für andere Datensätze wie "CROSSD", "BSI" und "CTQ". Dabei wird der entsprechende Ausschnitt des Datensatzes basierend auf dem Messzeitpunkt ausgewählt.

   Hier ist ein ähnliches Beispiel für "CROSSD":

   ```r
   CROSSD_dat <- reactive({
     if (messzeitpunkt() == "base") {
       subset(datensatz_patient(), select = c(Base_CROSSD_v1:Base_CROSSD_v12))
     } else if (messzeitpunkt() == "t0") {
       subset(datensatz_patient(), select = c(T0_CROSSD_v1:T0_CROSSD_v12))
     } else if (messzeitpunkt() == "t6") {
       subset(datensatz_patient(), select = c(T6_CROSSD_v1:T6_CROSSD_v12))
     } else if (messzeitpunkt() == "t12") {
       subset(datensatz_patient(), select = c(T12_CROSSD_v1:T12_CROSSD_v12))
     } else if (messzeitpunkt() == "t18") {
       subset(datensatz_patient(), select = c(T18_CROSSD_v1:T18_CROSSD_v12))
     } else if (messzeitpunkt() == "end") {
       subset(datensatz_patient(), select = c(End_CROSSD_v1:End_CROSSD_v12))
     } else {
       NULL
     }
   })
   ```

   Dieser Code ermöglicht es, Datensätze für verschiedene Messzeitpunkte auszuwählen.

3. **Berechnen von Skalen und Kennwerten**:

   Der Code berechnet verschiedene Skalen und Kennwerte aus den ausgewählten Datensätzen. Zum Beispiel werden Summenwerte, Durchschnittswerte und T-Werte für die BSI-Skalen berechnet. Der Code verwendet die `apply`-Funktion, um diese Berechnungen auf die ausgewählten Daten anzuwenden.

   Hier ist ein Beispiel für die Berechnung der BDI-II-Summenwerte:

   ```r
   BDI2 <- reactive(apply(BDI2_dat(), 1, sum, na.rm = FALSE))
   ```

   Dieser Code verwendet die `apply`-Funktion, um die Summe jeder Zeile im ausgewählten BDI-II-Datensatz zu berechnen.

4. **Umkodieren von Werten**:

   Der Code enthält auch Abschnitte, in denen W

erte umkodiert werden, z.B., die Umkodierung von BSI-Skalenwerten in T-Werte basierend auf dem Geschlecht der Person. Dies erfolgt mit Hilfe von `if`-Abfragen und Formeln.

   Hier ist ein Beispiel für die Umkodierung von BSI-Skalenwerten in T-Werte:

   ```r
   reactive({
     if (geschlecht() == "weiblich") {
       T_Wert = (BSI_Gn() - Mittelwert_w) / Standardabweichung_w
     } else {
       T_Wert = (BSI_Gn() - Mittelwert_m) / Standardabweichung_m
     }
   })
   ```

   In diesem Code werden die T-Werte für die BSI-Skalen basierend auf dem Geschlecht der Person berechnet.

#### Teil 1: Reaktive Datenrahmen erstellen

```R
x.na <- reactive(as.factor(input$pchiffre))
y.na <- reactive(c("hat den ausgewählten Messzeitpunkt noch nicht ausgefüllt"))
df.na <- reactive(data.frame(x.na(), y.na()))
```

- Hier werden reaktive Datenrahmen erstellt. Diese Datenrahmen sind reaktive in dem Sinne, dass sie sich automatisch aktualisieren, wenn sich die Benutzereingabe `input$pchiffre` ändert. `pchiffre` scheint ein Benutzereingabefeld in einer Shiny-App zu sein.

- `x.na` wird als `pchiffre`-Wert in einem Faktorformat erstellt.

- `y.na` ist ein Vektor mit einem Textelement, der anzeigt, dass der ausgewählte Messzeitpunkt noch nicht ausgefüllt wurde.

- `df.na` ist ein Datenrahmen, der `x.na` und `y.na` als Spalten enthält. Dieser Datenrahmen wird wahrscheinlich verwendet, um Benutzer darüber zu informieren, dass sie Daten eingeben müssen.

#### Teil 2: Bild für das Diagramm laden

```R
img <- readPNG("./www/logo_kM.png")
gpp <- rasterGrob(img, interpolate = TRUE)
```

- Hier wird ein Bild aus einer Datei namens "logo_kM.png" geladen. Dieses Bild wird in das Diagramm eingefügt.

- `readPNG` wird verwendet, um das Bild zu lesen und in der Variable `img` zu speichern.

- `rasterGrob` erstellt ein grafisches Objekt `gpp`, das das Bild in das Diagramm einbettet. `interpolate = TRUE` sorgt für eine glatte Darstellung des Bildes im Plot.

#### Teil 3: Erzeugen von Daten für Plots

```R
df <- reactive(data.frame(x = seq(1, 2, 0.01), y = seq(1, 2, 0.01)))
```

- Hier wird ein reaktiver Datenrahmen `df` erstellt. Dieser Datenrahmen enthält zwei Spalten: `x` und `y`.

- `x` und `y` sind Vektoren, die von 1 bis 2 in Schritten von 0,01 laufen. Dieser Datenrahmen wird wahrscheinlich verwendet, um den Hintergrund für einige der Plots zu generieren.

#### Teil 4: Erzeugen von Plots (Beispiel: BDI-2-Plot)

```R
BDI2.df <- reactive(data.frame(x = as.factor(input$pchiffre), y = BDI2()))
BDI2.plot <- reactive(ggplot(BDI2.df(), aes(x = x, y = y)) +
    geom_bar(stat = "identity", fill = if (BDI2() < 9) {"#04B431"} else if (BDI2() < 29) {"#FF8000"} else {"#FF0000"}, width = 0.3, alpha = 0.3) +
    # Weitere Plot-Anpassungen wie Beschriftungen und Titel
)
```

- Hier wird ein Beispiel für die Erstellung eines Plots gezeigt, nämlich des BDI-2-Plots.

- Zuerst wird ein reaktiver Datenrahmen `BDI2.df` erstellt, der `x` mit dem Benutzereingabewert `pchiffre` und `y` mit den BDI-2-Daten in reaktiver Funktion verknüpft.

- Dann wird ein Plot `BDI2.plot` mit `ggplot` erstellt. Dieser Balkendiagramm-Plot verwendet `BDI2.df`, und die Balkenfarbe wird basierend auf den Werten in `BDI2()` festgelegt.

- Der Plot wird angepasst, z. B. mit Achsenlimits, Linien und Textbeschriftungen.

Der Code erstellt ähnliche Plots für andere Messungen, wie PHQ-9, CROSS-D, BSI und CTQ. Die Struktur ist ähnlich, aber die Daten und Anpassungen sind unterschiedlich. In jedem Abschnitt für die verschiedenen Messungen werden spezifische Anpassungen an den Plots vorgenommen, wie das Hinzufügen von Textbeschriftungen, Anpassen der Farben und Achsenlimits sowie Hinzufügen von Linien zur Visualisierung von Schwellenwerten oder Cut-off-Werten.

Der Code erstellt eine Vielzahl von Plots für verschiedene psychologische Messungen und stellt die Daten in einer Shiny-App zur Verfügung, um Benutzern dabei zu helfen, die Ergebnisse dieser Messungen auf anschauliche Weise zu verstehen.



1. `CTQ.bagatell.df` und `CTQ.plot.bagatell`:
   - Hier wird ein Datenrahmen `CTQ.bagatell.df` definiert, der von der Eingabe `input$pchiffre` und einer Funktion `CTQ_bagatell()` abhängt. Dieser Datenrahmen wird für die Visualisierung verwendet.
   - `CTQ.plot.bagatell` ist eine Shiny-Reactive-Funktion, die ein ggplot-Diagramm erstellt. Es verwendet `CTQ.bagatell.df`, um Balkendiagramme und Textlabels zu erstellen.

2. Dataframes für Fragebögenoutput:
   - Hier werden Datenrahmen für verschiedene Fragebögen erstellt, wie `BDI2_FB.df`, `PHQ9_FB.df`, `CROSSD_FB.df`, `BSI_FB.df` und `CTQ_FB.df`. Diese Datenrahmen enthalten vermutlich die Daten aus den Fragebögen für weitere Verarbeitung und Visualisierung.

3. Verlaufsdaten:
   - Hier werden Datenrahmen für Verlaufsdaten erstellt, wie `bdi.ver.df`, `phq9.ver.df`, `crossd.ver.df`, `CTQ.ver.b`, `CTQ.ver.e`, usw. Diese Datenrahmen scheinen aggregierte Daten für verschiedene Fragebögen über den Verlauf der Zeit zu enthalten.

4. Visualisierungen:
   - Du erstellst mehrere Shiny-Reactive-Funktionen, um ggplot-Diagramme für Verlaufsdaten zu generieren. Zum Beispiel `bdi_ver`, `crossd_ver`, `phq9_ver`, `ctq_emo_ver` und `bsi_ver`. Je nach den Eingaben des Benutzers werden unterschiedliche Visualisierungen erstellt.


Sicher, ich helfe Ihnen gerne mit detaillierteren Codebeispielen. Es sieht so aus, als ob Sie R verwenden und einige Shiny-Reactive-Elemente haben. Ich werde jeden Abschnitt Ihres Codes mit Erläuterungen versehen und Beispiele für den jeweiligen Codebereich geben.

```R
CTQ.bagatell.df <- reactive({
  data.frame(x = as.factor(input$pchiffre), y = CTQ_bagatell())
})
```

In diesem Code wird eine Reactive-Expression namens `CTQ.bagatell.df` erstellt. Sie erstellt ein Datenframe mit den x- und y-Werten, wobei `x` von `input$pchiffre` abhängig ist und `y` von der Funktion `CTQ_bagatell()` kommt.

```R
CTQ.plot.bagatell <- reactive({
  ggplot(data = CTQ.bagatell.df(), aes(x = x, y = y)) +
    geom_bar(stat = "identity", fill = "#04B431", colour = "black", width = 0.3, alpha = 0.3) +
    geom_label(aes(label = CTQ_bagatell()), position = position_stack(vjust = 0.5), color = "black") +
    labs(x = input$pchiffre, y = "Wert") +
    coord_cartesian(ylim = c(0, 5), xlim = c(1, 1.5)) +
    scale_y_continuous(breaks = c(0, 1, 2, 3, 4, 5)) +
    scale_x_discrete(breaks = NULL) +
    annotate("text", label = "XXXXXXXX", x = 1.5, y = 2.5, size = 2, colour = "black") +
    annotate("text", label = "XXXXXXXX", x = 1.5, y = 7.5, size = 2, colour = "black") +
    annotate("text", label = "XXXXXXXX", x = 1.5, y = 12.5, size = 2, colour = "black") +
    annotate("text", label = "XXXXXXXX", x = 1.5, y = 21, size = 2, colour = "black") +
    theme_bw() +
    theme(plot.background = element_blank(),
          panel.grid.major.x = element_blank(),
          panel.grid.major.y = element_line(linetype = "dotted", colour = "black"),
          panel.grid.minor = element_blank(),
          panel.border = element_blank(),
          axis.title.y = element_text(vjust = 1)) +
    theme(axis.line = element_line(color = 'black')) +
    geom_hline(yintercept = 0)
})
```

Hier wird eine Reactive-Expression namens `CTQ.plot.bagatell` erstellt, die ein ggplot-Diagramm erstellt. Das Diagramm basiert auf den Daten von `CTQ.bagatell.df()` und enthält Balken mit `geom_bar`, Label mit `geom_label`, Achsentiteln, Anmerkungen und thematischen Einstellungen.

Bitte beachten Sie, dass die Funktionen `CTQ_bagatell()`, `BDI2_dat()`, `PHQ9_dat()`, `CROSSD_dat()`, `BSI_dat()`, `ctq.ver.b()`, `ctq.ver.e()`, `bsi.ver.b()`, `bsi.ver.t6()`, `bsi.ver.t12()`, `bsi.ver.t18()` und `bsi.ver.e()` im Code enthalten sind, aber ich habe nicht den Code für diese Funktionen. Sie sollten sicherstellen, dass diese Funktionen korrekt definiert und verfügbar sind.

**1. `output$htmlmarkdown` - Reactives HTML-Dokument**

```R
output$htmlmarkdown <- reactive({
  note_in_html(input$markdowninput)
})
```

Diese Ausgabe (`output$htmlmarkdown`) enthält ein reaktives HTML-Dokument basierend auf dem Eingabefeld `input$markdowninput`.

**2. `output$bdi_ver` - Reactives BDI2-Plot**

```R
output$bdi_ver <- renderPlot(bdi_ver())
```

Diese Ausgabe generiert ein reaktives Plot basierend auf der Funktion `bdi_ver()`. Sie generieren ein Diagramm mithilfe der `renderPlot`-Funktion, die auf den Ausgabewert von `bdi_ver()` reagiert.

**3. `output$test3` - Reactives BDI2-Plot oder NA-Plot**

```R
output$test3 <- renderPlot({
  if (is.na(BDI2() == TRUE)) {
    NA.plot()
  } else {
    BDI2.plot()
  }
})
```

Dieser Abschnitt erstellt ein reaktives Plot (`output$test3`), das abhängig von der Bedingung `is.na(BDI2() == TRUE)` entweder die Funktion `NA.plot()` oder `BDI2.plot()` aufruft. Es handelt sich um eine bedingte Anzeige eines Plots basierend auf dem Wert von `BDI2()`.

**4. `output$BDI2_wert` - Reactives DataTable für BDI2**

```R
output$BDI2_wert <- DT::renderDataTable({
  if (is.na(BDI2() == TRUE)) {
    df.na()
  } else {
    BDI2.df()
  }
})
```

Hier wird eine reaktive DataTable generiert, die basierend auf der Bedingung `is.na(BDI2() == TRUE)` entweder die Funktion `df.na()` oder `BDI2.df()` anzeigt. Die DataTable wird mit `DT::renderDataTable` erstellt.

Dieses Muster wird in den nächsten `output`-Definitionen wiederholt, wobei jeweils die entsprechenden Funktionen und Bedingungen angepasst werden.

**5. `output$BDI_FB` - DataTable mit Benutzeroberflächenerweiterungen für BDI-II**

```R
output$BDI_FB <- DT::renderDataTable(BDI2_FB.df(),
  colnames = c("Items", "Wert"),
  extensions = "Buttons",
  options = list(dom = 'Bfrtip', buttons = c('pdf'), pageLength = 21),
  rownames = TRUE,
  caption = htmltools::tags$caption(style = "caption-side: bottom; text-align: left;", "BDI-II: ", htmltools::em("Antworten des Patienten", input$pchiffre, "."))
)
```

Dieser Abschnitt erstellt eine DataTable mit Erweiterungen für die Benutzeroberfläche, die verschiedene Optionen wie PDF-Export und Styling bietet. Die DataTable basiert auf `BDI2_FB.df()` und enthält bestimmte Spaltennamen und Stile.

**6. `output$BDI2_Norm` - DataTable für BDI-II-Normdaten**

```R
output$BDI2_Norm <- DT::renderDataTable(BDI2.norm.df(),
  colnames = c("BDI2 Summerwert", "Bezeichnung"),
  rownames = FALSE
)
```

Hier wird eine DataTable erstellt, die auf `BDI2.norm.df()` basiert und die Spaltennamen festlegt. Die DataTable hat keine Zeilenbeschriftung (`rownames = FALSE`).

Dieselben Muster werden für die `output`-Definitionen `output$phq9_ver`, `output$test6`, `output$test5`, `output$PHQ9_FB`, `output$PHQ9_Norm`, `output$test7.2`, `output$test7.1`, `output$CROSSD_FB`, `output$CROSSD_Norm`, `output$crossd_ver`, `output$plot.bsi`, `output$test8.1`, `output$global` und `output$zusatz` verwendet. Beachten Sie, dass jeweils unterschiedliche Funktionen und Bedingungen in den `if`-Statements verwendet werden.

Das gesamte Shiny-App-Layout, die UI-Definitionen und die Server-Funktionen sollten auch im Code vorhanden sein, um diese `output`-Definitionen in einer Shiny-App ordnungsgemäß zu verwenden. Wenn Sie Fragen zu einem bestimmten Teil des Codes haben oder weitere Hilfe benötigen, lassen Sie es mich bitte wissen.

Der bereitgestellte R-Shiny-Code enthält eine Liste von `output$`-Definitionen, die DataTables und Plots für verschiedene Aspekte eines psychologischen Tests namens BSI (Brief Symptom Inventory) und andere verwandte Metriken erstellen. Hier werde ich jede `output$`-Definition erläutern und die dazugehörigen Codebeispiele bereitstellen:

**1. `output$BSI_FB` - DataTable für BSI (Brief Symptom Inventory) - Fragen und Antworten des Patienten**

```R
output$BSI_FB <- DT::renderDataTable(BSI_FB.df(),
  colnames = c("Wie sehr litten Sie in den letzten sieben Tagen unter ...?", "Wert"),
  extensions = "Buttons",
  options = list(dom = 'Bfrtip', buttons = c('pdf'), pageLength = 53),
  rownames = TRUE,
  caption = htmltools::tags$caption(style = "caption-side: bottom; text-align: left;", "BSI: ", htmltools::em("Antworten des Patienten", input$pchiffre, "Skala: Überhaupt nicht = 0; Ein wenig = 1; Ziemlich = 2; Stark = 3; Sehr stark = 4"))
)
```

Diese Ausgabe erstellt eine DataTable (`output$BSI_FB`) basierend auf den Daten aus `BSI_FB.df()`. Sie fügt verschiedene Benutzeroberflächenerweiterungen hinzu, darunter die Möglichkeit, die DataTable als PDF zu exportieren. Die DataTable zeigt Fragen und Antworten des Patienten für den BSI-Test und enthält Spalten für die Fragen und die zugehörigen Werte, die auf einer Skala von 0 bis 4 bewertet werden.

**2. `output$BSI_Norm.soma` bis `output$BSI_Norm.gsi` - Normtabellen für verschiedene Skalen des BSI**

Die folgenden `output$`-Definitionen erstellen DataTables für verschiedene Normtabellen basierend auf den Skalen des BSI:

- `output$BSI_Norm.soma`: Normtabelle für die Somatisierungsskala.
- `output$BSI_Norm.zwang`: Normtabelle für die Zwanghaftigkeitsskala.
- `output$BSI_Norm.unsich`: Normtabelle für die Unsicherheitsskala.
- `output$BSI_Norm.soma`: Normtabelle für die Depressivitätsskala.
- `output$BSI_Norm.angts`: Normtabelle für die Ängstlichkeitsskala.
- `output$BSI_Norm.agg`: Normtabelle für die Aggressivitätsskala.
- `output$BSI_Norm.phob`: Normtabelle für die Phobieskala.
- `output$BSI_Norm.paran`: Normtabelle für die Paranoiditätsskala.
- `output$BSI_Norm.psych`: Normtabelle für die Psychotizismusskala.
- `output$BSI_Norm.psdi`: Normtabelle für die Phobische Angstskala.
- `output$BSI_Norm.pst`: Normtabelle für die Somatische Beschwerdendisplay.

Alle diese Ausgaben verwenden den `DT::renderDataTable`, um DataTables basierend auf den jeweiligen Datenquellen (`BSI.norm.soma.df()`, `BSI.norm.zwang.df()`, usw.) zu erstellen. Sie enthalten unterschiedliche Spalten und Beschriftungen für die entsprechende Skala.

**3. `output$bsi_ver` - Reaktives BSI-Plot**

```R
output$bsi_ver <- renderPlot(bsi_ver())
```

Diese Ausgabe erstellt ein reaktives Plot basierend auf der Funktion `bsi_ver()`. Das Plot wird mithilfe von `renderPlot` erstellt.

**4. `output$test9.11` - BSI-Plot oder NA-Plot**

```R
output$test9.11 <- renderPlot({
  (CTQ.plot())
})
```

Diese Ausgabe erstellt ein reaktives Plot (`output$test9.11`), das entweder die Funktion `CTQ.plot()` oder `NA.plot()` abhängig von der Bedingung `is.na(CTQ.y() == TRUE)` anzeigt. Dies ist eine bedingte Anzeige eines Plots basierend auf dem Wert von `CTQ.y()`.

**5. `output$test9.2` - DataTable für CTQ-Daten**

```R
output$test9.2 <- DT::renderDataTable({
  (CTQ.df())
})
```

Hier wird eine reaktive DataTable generiert, die auf die Bedingung `is.na(CTQ.y() == TRUE)` reagiert und basierend auf dieser Bedingung entweder `df.na()` oder `CTQ.df()` anzeigt.

**6. `output$CTQ_FB` - DataTable für CTQ-Daten mit Benutzeroberflächenerweiterungen**

```R
output$CTQ_FB <- DT::renderDataTable(CTQ_FB.df(),
  extensions = "Buttons",
  options = list(dom = 'Bfrtip', buttons = c('pdf'), pageLength = 21),
  rownames = FALSE,
  caption = htmltools::tags$caption(style = "caption-side: bottom; text-align: left;", "CTQ: ", htmltools::em("Antworten des Patienten", input$pchiffre, "Skala: Überhaupt nicht = 1; Sehr selten = 2; Einige Male = 3; Häufig = 4, Sehr häufig = 5"))
)
```

Diese Ausgabe erstellt eine DataTable für CTQ-Daten mit Benutzeroberflächenerweiterungen, die das Exportieren als PDF ermöglichen. Die DataTable zeigt die Antworten des Patienten auf Fragen, wobei die Skala von 1 bis 5 reicht.

**7. `output$CTQ_Norm` - DataTable für CTQ-Normdaten**

```R
output$CTQ_Norm <- DT::renderDataTable(CTQ.norm.df(),
  rownames = c("emotionaler Missbrauch" ,"körperlicher Missbrauch","Sexueller Missbrauch", "emotionale Vernachlässigung", "körperliche Vernachlässigung"),
  colnames = c("nicht bis minimal","gering bis mäßig","mäßig bis schwer"),
  caption = htmltools::tags$caption(style = "caption-side: bottom; text-align: left;", "Eine Misshandlung wird ab einem Schweregrad gering/mäßig angenommen")
)
```

Hier wird eine DataTable für CTQ-Normdaten erstellt. Die DataTable enthält spezifische Zeilen- und Spaltenbeschriftungen. Schließlich gibt es einige `output$`-Definitionen für "Verlauf" und "missing" (fehlende Daten), bei denen weitere Plots und Tabellen generiert werden, um den Verlauf und mögliche fehlende Daten darzustellen. Der Code zeigt die Erstellung von DataTables und Plots in einer Shiny-App-Umgebung. Bitte beachten Sie, dass der Code in einer vollständigen Shiny-App-Umgebung mit entsprechenden UI- und Server-Teilen verwendet werden sollte, um die Anwendung zu starten. Der Code enthält zwei `observeEvent`-Blöcke und eine Ausgabe für das Herunterladen eines Berichts. Hier werde ich die Verwendung und den Zweck dieser Blöcke im Detail erläutern:

**1. `observeEvent(input$messzeitpunkt, ...)` - Anzeigen und Ausblenden von Tabs in der Benutzeroberfläche**

Dieser `observeEvent`-Block wird ausgelöst, wenn sich der Wert des Eingabefelds `input$messzeitpunkt` ändert. Der Block hat den Zweck, bestimmte Tabs in der Shiny-Anwendung anzuzeigen oder auszublenden, je nachdem, welcher Messzeitpunkt ausgewählt wurde.

```R
observeEvent(input$messzeitpunkt, {
  if (messzeitpunkt() == "base") {
    showTab(inputId = "ergebnisse", target = "BDI-2")
    showTab(inputId = "ergebnisse", target = "BSI")
    showTab(inputId = "ergebnisse", target = "CTQ")
  } else if (messzeitpunkt() == "t0") {
    hideTab(inputId = "ergebnisse", target = "BDI-2")
    hideTab(inputId = "ergebnisse", target = "BSI")
    hideTab(inputId = "ergebnisse", target = "CTQ")
  } else if (messzeitpunkt() == "t6") {
    hideTab(inputId = "ergebnisse", target = "BDI-2")
    showTab(inputId = "ergebnisse", target = "BSI")
    hideTab(inputId = "ergebnisse", target = "CTQ")
  } else if (messzeitpunkt() == "t12") {
    hideTab(inputId = "ergebnisse", target = "BDI-2")
    showTab(inputId = "ergebnisse", target = "BSI")
    hideTab(inputId = "ergebnisse", target = "CTQ")
  } else if (messzeitpunkt() == "t18") {
    hideTab(inputId = "ergebnisse", target = "BDI-2")
    showTab(inputId = "ergebnisse", target = "BSI")
    hideTab(inputId = "ergebnisse", target = "CTQ")
  } else if (messzeitpunkt() == "end") {
    showTab(inputId = "ergebnisse", target = "BDI-2")
    showTab(inputId = "ergebnisse", target = "BSI")
    showTab(inputId = "ergebnisse", target = "CTQ")
  } else {
    # Keine Aktion erforderlich
  }
})
```

Dieser Code verwendet die Funktionen `showTab` und `hideTab`, um Tabs in der Shiny-Benutzeroberfläche einzublenden oder auszublenden. Die Auswahl des richtigen Tabs erfolgt basierend auf dem Wert von `messzeitpunkt()`, der von einem anderen Eingabefeld abhängt. Die Logik variiert je nach gewähltem Messzeitpunkt, und es werden unterschiedliche Tabs angezeigt oder ausgeblendet.

**2. `output$report` - Herunterladen eines Berichts im PDF- oder Word-Format**

Diese Ausgabe ermöglicht dem Benutzer das Herunterladen eines Berichts im PDF- oder Word-Format. Der Code enthält zwei separate Abschnitte für die Erstellung des Berichts im PDF- oder Word-Format.

- Für das PDF-Format wird `rmarkdown::render` verwendet, um ein R Markdown-Dokument zu rendern und in eine PDF-Datei auszugeben. Die R Markdown-Dokumente (z.B., "report_base_pdf.Rmd") enthalten Platzhalter für verschiedene Plots und Text, die in der Datei `params` definiert sind. Der Name der heruntergeladenen Datei wird basierend auf dem ausgewählten Messzeitpunkt und dem gewünschten Format generiert.

- Für das Word-Format erfolgt der Vorgang ähnlich wie beim PDF, jedoch mit `output_file` für das Word-Format. Auch hier werden R Markdown-Dokumente verwendet, um den Bericht zu erstellen.

Bitte beachten Sie, dass die `params` in den R Markdown-Dokumenten (z.B., "report_base_pdf.Rmd") definiert werden müssen, um die Plots und den Text für den Bericht korrekt einzufügen. Dies ermöglicht es, Berichte basierend auf den ausgewählten Einstellungen (Messzeitpunkt und Format) zu generieren und herunterzuladen.

Dieser Codeblock ermöglicht es dem Benutzer, Berichte in verschiedenen Formaten herunterzuladen, je nachdem, welche Optionen sie auswählen.


1. **R-Markdown auf Tabset**

Die folgenden beiden Abschnitte des Codes sind verantwortlich für die Anzeige von R-Markdown-Berichten auf verschiedenen Tab-Reitern. Die Berichte werden in den R-Markdown-Dateien "page1.Rmd" und "page2.Rmd" definiert und auf den entsprechenden Tabs angezeigt:

```R
output$page1 <- renderUI({
  inclRmd("./page1.Rmd")
})

output$page2 <- renderUI({
  inclRmd("./page2.Rmd")
})
```

In diesem Code wird `renderUI` verwendet, um das R-Markdown-Dokument mithilfe der `inclRmd`-Funktion anzuzeigen. Dies ermöglicht die Darstellung von Berichten und Informationen in der Shiny-App auf verschiedenen Tabs.

2. **Benutzerauthentifizierung mit Passwörtern**

Der folgende Codeblock behandelt die Benutzerauthentifizierung mithilfe von Benutzernamen und Passwörtern:

```R
user_input <- reactiveValues(authenticated = FALSE, valid_credentials = FALSE, user_locked_out = FALSE, status = "")

observeEvent(input$login_button, {
  # Laden der Anmeldeinformationen aus einer RDS-Datei
  credentials <- readRDS("credentials/credentials.rds")

  # Suchen des Benutzernamens in den Anmeldeinformationen
  row_username <- which(credentials$user == input$user_name)
  row_password <- which(credentials$pw == digest(input$password))  # Das Passwort wird als MD5-Hash gespeichert

  if (length(row_username) == 1 &&
      length(row_password) >= 1 &&
      (row_username %in% row_password)) {
    user_input$valid_credentials <- TRUE
    user_input$user_locked_out <- credentials$locked_out[row_username]
  }

  if (input$login_button == num_fails_to_lockout &&
      user_input$user_locked_out == FALSE) {
    user_input$user_locked_out <- TRUE
    if (length(row_username) == 1) {
      credentials$locked_out[row_username] <- TRUE
      saveRDS(credentials, "credentials/credentials.rds")
    }
  }

  if (user_input$valid_credentials == TRUE & user_input$user_locked_out == FALSE) {
    user_input$authenticated <- TRUE
  } else {
    user_input$authenticated <- FALSE
  }

  if (user_input$authenticated == FALSE) {
    if (user_input$user_locked_out == TRUE) {
      user_input$status <- "locked_out"
    } else if (length(row_username) > 1) {
      user_input$status <- "credentials_data_error"
    } else if (input$user_name == "" || length(row_username) == 0) {
      user_input$status <- "bad_user"
    } else if (input$password == "" || length(row_password) == 0) {
      user_input$status <- "bad_password"
    }
  }
})
```

In diesem Code werden die Benutzernamen und Passwörter validiert und überprüft. Eine RDS-Datei namens "credentials.rds" enthält die Benutzernamen und Passwörter, die mithilfe der `digest`-Funktion in MD5-Hashes umgewandelt werden. Der Benutzer wird authentifiziert, wenn die Anmeldeinformationen gültig sind und er nicht gesperrt ist.

3. **UI-Komponenten für die Passworteingabe**

Die folgenden UI-Komponenten zeigen Textfelder für Benutzernamen und Passwörter sowie eine Schaltfläche zum Anmelden an:

```R
output$uiLogin <- renderUI({
  wellPanel(
    textInput("user_name", "Benutzername:"),
    passwordInput("password", "Passwort:"),
    actionButton("login_button", "Anmelden")
  )
})
```

Diese Komponenten ermöglichen es dem Benutzer, seinen Benutzernamen und sein Passwort einzugeben und sich anzumelden.

4. **Abmelden**

Die folgende Codezeile reagiert auf das Klicken der Schaltfläche "Abmelden" und setzt die Benutzerauthentifizierungsvariablen zurück:

```R
observeEvent(input$logout, {
  user_input$authenticated <- FALSE
  user_input$valid_credentials <- FALSE
  user_input$user_locked_out <- FALSE
  user_input$status <- ""
  shinyalert("Sie haben sich erfolgreich ausgeloggt.", type = "success")
})
```

Dieser Codeblock wird verwendet, um den Benutzer abzumelden und alle Authentifizierungsvariablen zurückzusetzen.

5. **Fehlermeldungen anzeigen**

Der folgende Code zeigt Fehlermeldungen basierend auf dem Authentifizierungsstatus und den eingegebenen Informationen:

```R
output$pass <- renderUI({
  if (user_input$status == "locked_out") {
    shinyalert(title = "Benutzer gesperrt! - Bitte Admin kontaktieren", type = "error")
  } else if (user_input$status == "credentials_data_error") {
    shinyalert(title = "Credentials data error - kontaktieren Sie den Administrator!", type = "error")
  } else if (user_input$status == "bad_user") {
    shinyalert(title = "Benutzername nicht gefunden", type = "error")
  } else if (user_input$status == "bad_password") {
    shinyalert(title = "Passwort nicht korrekt", type = "error")
  } else if (user_input$authenticated == TRUE) {
    shinyalert(title = "Herzlich Willkommen", type = "success")
  } else {}
})
```

In diesem Code werden Fehlermeldungen angezeigt, wenn die Authentifizierung fehlschlägt oder wenn bestimmte Bedingungen nicht erfüllt sind. Je nach Authentifizierungsstatus werden unterschiedliche Meldungen angezeigt, einschließlich erfolgreicher Anmeldung oder Fehlermeldungen, die auf das Problem hinweisen. Die Meldungen werden mithilfe der `shinyalert`-Funktion dargestellt.






