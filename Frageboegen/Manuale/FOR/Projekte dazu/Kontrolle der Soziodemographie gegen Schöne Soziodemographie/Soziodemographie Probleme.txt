(Mögliche) Probleme:
	- Geschlecht gleich kodiert? Wie ist das bei uns? Ist nicht im Soziodemographie Bogen, wird woanders erfasst. Wir unterscheiden bestimmt nicht zwischen "Kein Geschlecht" und "Divers"
	- Prior Treatment wird bei uns wesentlich komplexer erfasst, auf letzte 5 Jahre bezogen statt allgemein
	- Ein paar Variablen müssen gemapt werden (marital_status, graduation, profession, employability, prior_treatment), manche Mappings sind dabei nicht 100% eindeutig
	
Nicht erfasst:
	- Years of Education
	- Der ganze Bereich (spezifisch) psychotrope Medikamente
		Wir haben stattdessen allgemein Medikamentenangabe mit täglicher Dosis, keine Bedarfsmedikamente
	- Der ganze Bereich psychische Erkrankungen Familie
		Familie (Eltern und Geschwister) wird abgefragt, auch mit Erkrankungen, nicht spezifisch psychisch
	- SÖS