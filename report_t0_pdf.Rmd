---
title: "Auswertung erste bewilligte Sitzung"
output: pdf_document

params:
  
  plot2: NA
  plot5: NA
  text: NA

---

## Plots


Im Folgenden findet sich eine Zusammenfassung der Testauswertungen zur Messung nach der ersten bewilligten Sitzung für den Patienten


```{r}

params$plot2

```

```{r}

params$plot5

```

```{r Interval2, warning = FALSE}
note_in_md_pdf(params[["text"]])

```



