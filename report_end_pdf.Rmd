---
title: "Auswertung nach Beendigung"
output: pdf_document

params:
  
  plot1: NA
  plot2: NA
  plot3: NA
  plot4: NA
  plot5: NA
  plot6: NA
  text: NA
---

### Plots und Werte der einzelnen Fragebögen

***
<br><br><br><br>

Im Folgenden findet sich eine Zusammenfassung der Testauswertungen zur Endmessung für den  Patienten


``` {r out.width=c('50%', '50%'), fig.show='hold'}
params$plot1
params$plot2

```


```{r, fig.height = 4, fig.width = 7, fig.align = "center"}

params$plot5

```

```{r, fig.height = 5, fig.width = 8, fig.align = "center"}

params$plot3

```

```{r, fig.height = 5, fig.width = 8, fig.align = "center"}

params$plot4

```

```{r, fig.height = 5, fig.width = 8, fig.align = "center"}

params$plot6

```

```{r Interval2, warning = FALSE}
note_in_md_pdf(params[["text"]])
```

